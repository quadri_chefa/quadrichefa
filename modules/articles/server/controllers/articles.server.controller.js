'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Article = mongoose.model('Article'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

//var NodeCache = require( "node-cache" );
//var myCache = new NodeCache( { stdTTL: 7200, checkperiod: 7400 } );

/**
 * Create a article
 */
exports.create = function (req, res) {
  var article = new Article(req.body);
  article.user = req.user;

  article.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(article);
    }
  });
};

/**
 * Show the current article
 */
exports.read = function (req, res) {
  res.json(req.article);
};

/**
 * Update a article
 */
exports.update = function (req, res) {
  var article = req.article;

  article.title = req.body.title;
  article.content = req.body.content;

  article.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(article);
    }
  });
};

/**
 * Delete an article
 */
exports.delete = function (req, res) {
  var article = req.article;

  article.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(article);
    }
  });
};

/**
 * List of Articles
 */
exports.list = function (req, res) {
  Article.find().sort('-created').populate('user', 'displayName').exec(function (err, articles) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(articles);
    }
  });
};

/**
 * Article middleware
 */
exports.articleByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Article is invalid'
    });
  }

  Article.findById(id).populate('user', 'displayName').exec(function (err, article) {
    if (err) {
      return next(err);
    } else if (!article) {
      return res.status(404).send({
        message: 'No article with that identifier has been found'
      });
    }
    req.article = article;
    next();
  });
};


/**
 * getvideo http://diveintohtml5.info/video.html
 */
exports.getvideo = function (req,res) {
  

 var total='377759'; 
//http://quadrichefa-001.herokuapp.com
  var hostname= req.hostname.replace('.herokuapp.com','').replace('http://','').replace('-','');
  var filename=hostname+'.mp4';
  console.log('req.hostname: '+req.hostname);


if(req.url.indexOf('download.mp4')!==-1){
  var mongoose = require('mongoose');
  var Grid = require('gridfs-stream');
  Grid.mongo = mongoose.mongo;
  var conn = mongoose.createConnection('mongodb://quadrichefa:quadrichefa@ds055792.mongolab.com:55792/quadrichefa');
  conn.once('open', function () {
  console.log('conn open and ready to download!');
  


  
  var gfs = Grid(conn.db);
    var readstream = gfs.createReadStream({
  filename: filename
  });
  res.writeHead(200, {'Content-Type': 'video/mp4','Cache-Control': 'public, max-age=3600'});
	   
   readstream.on('data', function(data) {
	        res.write(data);
	    });
	    
	 readstream.on('end', function() {
	        
	        res.end();        
	    });

	 readstream.on('error', function (err) {
		  console.log('An error occurred!', err);
		  res.send('An error occurred!');
		});
  });
} 
else{
  // GET FROM CACHE
//myCache.get(hostname, function( err, value ){
   
  var mongoose = require('mongoose');
  var Grid = require('gridfs-stream');
  Grid.mongo = mongoose.mongo;
  var conn = mongoose.createConnection('mongodb://quadrichefa:quadrichefa@ds055792.mongolab.com:55792/quadrichefa');
  conn.once('open', function () {
  console.log('conn open!');
  
/***
   gfs.exist({_id: req.query.id+''}, function (err, found) {
  if (err) return handleError(err);
  found ? console.log('File exists') : console.log('File does not exist');
  res.send(found)
});
*/  
 
  
  var gfs = Grid(conn.db);
  var gfs2 = Grid(conn.db);
    gfs.findOne({ filename: filename}, function (err, file) {
      
    if (!err) {
    console.log(file);
    total=file.length;
    
    
  var range = 'bytes=48-'; 
  if (req.headers['range']) {
    range = req.headers.range; 
  }
  //// var range = req.headers.range; 
    console.log("Range = "+range);
   
    var parts = range.replace(/bytes=/, "").split("-");
    var partialstart = parts[0];
    var partialend = parts[1];
    var start = parseInt(partialstart, 10);
    var end = partialend ? parseInt(partialend, 10) : total-1; 
    var chunksize = (end-start)+1;
    console.log('RANGE: ' + start + ' - ' + end + ' = ' + chunksize);

   
  var readstream = gfs2.createReadStream({
  filename: filename,
    range: {
    startPos: start,
    endPos: end
    }
  });

//res.setHeader("Access-Control-Allow-Origin", "*");
//res.setHeader("Content-Type", "video/mp4");
//res.setHeader("Cache-Control", "public, max-age=7200");
/*
res.writeHead(200, {'Content-Type': 'video/mp4','Cache-Control': 'public, max-age=3600'});
	   
   readstream.on('data', function(data) {
	        res.write(data);
	    });
	    
	 readstream.on('end', function() {
	        
	        res.end();        
	    });

	 readstream.on('error', function (err) {
		  console.log('An error occurred!', err);
		  res.send('An error occurred!');
		});
	
		*/
		
	/*	
		var obj = { mytotal: total, myreadstream: readstream };
  myCache.set( hostname, obj, function( err, success ){
  if( !err && success ){
    console.log( success );
    // true 
    // ... do something ... 
  }
});	
*/
		 res.writeHead(206, { 'Content-Range': 'bytes ' + start + '-' + end + '/' + total, 'Accept-Ranges': 'bytes', 'Content-Length': chunksize, 'Content-Type': 'video/mp4' });
    readstream.pipe(res);

		
    
  //////////////}
//////  else{
  
////////////   res.end();
   /* 
        console.log('ALL: ' + total);
    //res.writeHead(200, { 'Content-Length': total, 'Content-Type': 'video/mp4' });
    //fs.createReadStream(path).pipe(res);
    res.writeHead(200, {'Content-Type': 'video/mp4','Cache-Control': 'public, max-age=3600'});
	   
   readstream.on('data', function(data) {
	        res.write(data);
	    });
	    
	 readstream.on('end', function() {
	        
	        res.end();        
	    });

	 readstream.on('error', function (err) {
		  console.log('An error occurred!', err);
		  res.send('An error occurred!');
		});
	*/
  /////////////}
    }
    else{
       res.end();
    }
  });
  
 
});
   
 
//});
  }
  
  /*
     Article.findOne().exec(function(err, quadricamstored) {//.sort('-created').select('categoryid categoryname')
    if (err) {
      return res.status(500).json({
        error: 'Cannot list the tvcontentscategories'
      });
    }
    //res.json(tvcatalogs);
        console.log('videotape: '+quadricamstored.videotape.toString('base64'));
       
HTTP/1.1 206 Partial Content
Connection: keep-alive
Date: Thu, 27 Aug 2015 14:29:21 GMT
Server: Apache/2.4.10 (Unix)
Last-Modified: Fri, 10 Jul 2015 15:29:49 GMT
Etag: "845bfe-51a870ae15540"
Via: 1.1 vegur
Access-Control-Allow-Origin: *
X-Download-Options: noopen

    res.send(quadricamstored.videotape);
*/
      
  //});
  
  

  
  
  
  
  /*
  
    var fs = require('fs');
    var file = path.resolve('video/video.mp4');
    var range = req.headers.range;
    var positions = range.replace(/bytes=/, '').split('-');
    var start = parseInt(positions[0], 10);

    fs.stat(file, function(err, stats) {
      var total = stats.size;
      var end = positions[1] ? parseInt(positions[1], 10) : total - 1;
      var chunksize = (end - start) + 1;

      res.writeHead(206, {
        'Content-Range': 'bytes ' + start + '-' + end + '/' + total,
        'Accept-Ranges': 'bytes',
        'Content-Length': chunksize,
        'Content-Type': 'video/mp4'
      });

      var stream = fs.createReadStream(base64, { start: start, end: end })
        .on('open', function() {
          stream.pipe(res);
        }).on('error', function(err) {
          res.end(err);
        });
       
       
    });
    */
};


/**
 * getwebmvideo http://diveintohtml5.info/video.html
 */
exports.getwebmvideo = function (req,res)
{
    var fs = require('fs');
    var file = path.resolve('video/video.webm');
    var range = req.headers.range;
    var positions = range.replace(/bytes=/, '').split('-');
    var start = parseInt(positions[0], 10);

    fs.stat(file, function(err, stats) {
      var total = stats.size;
      var end = positions[1] ? parseInt(positions[1], 10) : total - 1;
      var chunksize = (end - start) + 1;

      res.writeHead(206, {
        'Content-Range': 'bytes ' + start + '-' + end + '/' + total,
        'Accept-Ranges': 'bytes',
        'Content-Length': chunksize,
        'Content-Type': 'video/webm'
      });

      var stream = fs.createReadStream(file, { start: start, end: end })
        .on('open', function() {
          stream.pipe(res);
        }).on('error', function(err) {
          res.end(err);
        });
    });
};


/**
 *
 * getretrofitcatalogcam
 */
 
exports.getretrofitcatalogcam = function (req, res) {

res.setHeader("Access-Control-Allow-Origin", "*");
res.setHeader("Content-Type", "application/json");
res.setHeader("Cache-Control", "public, max-age=7200");
res.end('{"results":[{"_id": "8600600124dac4bc3d6887ca","categoryimageurl": "http://www.inabruzzo.it/wp-content/uploads/2010/03/quadri.jpg","categoryname": "QUADRI","categoryid": "quadri","title": "quadri web cam","imageurl": "http://www.inabruzzo.it/wp-content/uploads/2010/03/quadri.jpg","streamurl": "http://quadrichefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887cb","categoryimageurl": "http://www.abruzzocitta.it/foto/comuni/borrello1.jpg","categoryname": "BORRELLO","categoryid": "borrello","title": "borrello web cam","imageurl": "http://www.abruzzocitta.it/foto/comuni/borrello1.jpg","streamurl": "http://borrellochefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887cc","categoryimageurl": "http://farm3.static.flickr.com/2712/4268719344_a4f43af9a7.jpg","categoryname": "CIVITALUPARELLA","categoryid": "civitaluparella","title": "civitaluparella web cam","imageurl": "http://farm3.static.flickr.com/2712/4268719344_a4f43af9a7.jpg","streamurl": "http://civitaluparellachefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887cd","categoryimageurl": "https://upload.wikimedia.org/wikipedia/commons/2/2c/Piazza_Plebiscito_-_Lanciano.jpg","categoryname": "LANCIANO","categoryid": "lanciano","title": "lanciano web cam","imageurl": "https://upload.wikimedia.org/wikipedia/commons/2/2c/Piazza_Plebiscito_-_Lanciano.jpg","streamurl": "http://lancianochefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887ce","categoryimageurl": "https://upload.wikimedia.org/wikipedia/commons/a/a7/Pizzoferrato.jpg","categoryname": "PIZZOFERRATO","categoryid": "pizzoferrato","title": "pizzoferrato web cam","imageurl": "https://upload.wikimedia.org/wikipedia/commons/a/a7/Pizzoferrato.jpg","streamurl": "http://pizzoferratochefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887cf","categoryimageurl": "http://abruzzoholidayinformation.com/Places_In_Abruzzo/AtessaAA.JPG","categoryname": "ATESSA","categoryid": "atessa","title": "atessa web cam","imageurl": "http://abruzzoholidayinformation.com/Places_In_Abruzzo/AtessaAA.JPG","streamurl": "http://atessachefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887d0","categoryimageurl": "https://farm3.staticflickr.com/2744/4128797728_718e24ac1f_z.jpg?zz=1","categoryname": "TORRICELLA PELIGNA","categoryid": "torricella","title": "torricella peligna web cam","imageurl": "https://farm3.staticflickr.com/2744/4128797728_718e24ac1f_z.jpg?zz=1","streamurl": "http://torricellachefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887d1","categoryimageurl": "http://www.altosannio.it/wp-content/uploads/2013/06/pescopennatarro.jpg","categoryname": "PESCOPENNATARO","categoryid": "torricella","title": "pescopennataro web cam","imageurl": "http://www.altosannio.it/wp-content/uploads/2013/06/pescopennatarro.jpg","streamurl": "http://pescopennatarochefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887d2","categoryimageurl": "https://upload.wikimedia.org/wikipedia/commons/6/62/Casoli_01.jpg","categoryname": "CASOLI","categoryid": "casoli","title": "casoli web cam","imageurl": "https://upload.wikimedia.org/wikipedia/commons/6/62/Casoli_01.jpg","streamurl": "http://casolichefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887d3","categoryimageurl": "http://allworldtowns.com/data_images/countries/vasto/vasto-01.jpg","categoryname": "VASTO","categoryid": "vasto","title": "vasto web cam","imageurl": "http://allworldtowns.com/data_images/countries/vasto/vasto-01.jpg","streamurl": "http://vastochefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"}]}');


};

/**
 *
 * getnewcatalogcam
 */
 
exports.getnewcatalogcam = function (req, res) {

res.setHeader("Access-Control-Allow-Origin", "*");
res.setHeader("Content-Type", "application/json");
res.setHeader("Cache-Control", "public, max-age=7200");
res.end('[{"_id": "8600600124dac4bc3d6887ca","categoryimageurl": "http://www.inabruzzo.it/wp-content/uploads/2010/03/quadri.jpg","categoryname": "QUADRI","categoryid": "quadri","title": "quadri web cam","imageurl": "http://www.inabruzzo.it/wp-content/uploads/2010/03/quadri.jpg","streamurl": "http://quadrichefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887cb","categoryimageurl": "http://www.abruzzocitta.it/foto/comuni/borrello1.jpg","categoryname": "BORRELLO","categoryid": "borrello","title": "borrello web cam","imageurl": "http://www.abruzzocitta.it/foto/comuni/borrello1.jpg","streamurl": "http://borrellochefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887cc","categoryimageurl": "http://farm3.static.flickr.com/2712/4268719344_a4f43af9a7.jpg","categoryname": "CIVITALUPARELLA","categoryid": "civitaluparella","title": "civitaluparella web cam","imageurl": "http://farm3.static.flickr.com/2712/4268719344_a4f43af9a7.jpg","streamurl": "http://civitaluparellachefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887cd","categoryimageurl": "https://upload.wikimedia.org/wikipedia/commons/2/2c/Piazza_Plebiscito_-_Lanciano.jpg","categoryname": "LANCIANO","categoryid": "lanciano","title": "lanciano web cam","imageurl": "https://upload.wikimedia.org/wikipedia/commons/2/2c/Piazza_Plebiscito_-_Lanciano.jpg","streamurl": "http://lancianochefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887ce","categoryimageurl": "https://upload.wikimedia.org/wikipedia/commons/a/a7/Pizzoferrato.jpg","categoryname": "PIZZOFERRATO","categoryid": "pizzoferrato","title": "pizzoferrato web cam","imageurl": "https://upload.wikimedia.org/wikipedia/commons/a/a7/Pizzoferrato.jpg","streamurl": "http://pizzoferratochefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887cf","categoryimageurl": "http://abruzzoholidayinformation.com/Places_In_Abruzzo/AtessaAA.JPG","categoryname": "ATESSA","categoryid": "atessa","title": "atessa web cam","imageurl": "http://abruzzoholidayinformation.com/Places_In_Abruzzo/AtessaAA.JPG","streamurl": "http://atessachefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887d0","categoryimageurl": "https://farm3.staticflickr.com/2744/4128797728_718e24ac1f_z.jpg?zz=1","categoryname": "TORRICELLA PELIGNA","categoryid": "torricella","title": "torricella peligna web cam","imageurl": "https://farm3.staticflickr.com/2744/4128797728_718e24ac1f_z.jpg?zz=1","streamurl": "http://torricellachefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887d1","categoryimageurl": "http://www.altosannio.it/wp-content/uploads/2013/06/pescopennatarro.jpg","categoryname": "PESCOPENNATARO","categoryid": "torricella","title": "pescopennataro web cam","imageurl": "http://www.altosannio.it/wp-content/uploads/2013/06/pescopennatarro.jpg","streamurl": "http://pescopennatarochefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887d2","categoryimageurl": "https://upload.wikimedia.org/wikipedia/commons/6/62/Casoli_01.jpg","categoryname": "CASOLI","categoryid": "casoli","title": "casoli web cam","imageurl": "https://upload.wikimedia.org/wikipedia/commons/6/62/Casoli_01.jpg","streamurl": "http://casolichefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"},{"_id": "8600600124dac4bc3d6887d3","categoryimageurl": "http://allworldtowns.com/data_images/countries/vasto/vasto-01.jpg","categoryname": "VASTO","categoryid": "vasto","title": "vasto web cam","imageurl": "http://allworldtowns.com/data_images/countries/vasto/vasto-01.jpg","streamurl": "http://vastochefa-001.herokuapp.com/api/getvideo?cam.mp4","duration": "00:00:06","viewers": 631,"__v": 0,"created": "2015-09-29T22:04:15.230Z"}]');
};

/**
 * getvideocam
 */
exports.getvideocam= function (req, res){
  
  //http://quadrichefa-001.herokuapp.com
    var hostname= req.hostname.replace('.herokuapp.com','').replace('http://','').replace('-','');
    var filename=hostname+'.mp4';
    
    //var filename='quadrichefa001'+'.mp4';
 //  var filename='borrellochefa001'+'.mp4';
  //  var filename='civitaluparellachefa001'+'.mp4';
//    var filename='lancianochefa001'+'.mp4';
    console.log('req.hostname: '+req.hostname);
    var imagesdir='images/';
     var fs = require('fs');
     
     
     
      fs.readdir(imagesdir, function (err, files) {
        
  if (err) throw err;

 if (files.length>2) {
  
     
    /*
    
      each five minutes:
      - download 5 images cam0001 cam0002 cam0003 cam0004 cam0005 set global variable: lastimage cam0005 
  
      each 120 mins
      - remove olds images (older than 7 days)
   
   
     var fs = require('fs');
      fs.unlinkSync('video/video.mp4');
        fs.unlinkSync('video/video.webm');
      console.log('deleted');
   */  
    var avconv = require('avconv');
    //avconv -i foo.avi -r 1 -s WxH -f image2 foo-%03d.jpeg
    //avconv -f image2 -i foo-%03d.jpeg -r 12 -s WxH foo.avi
    //avconv -r 6 -qscale 1 -i %05d.morph.jpg -vcodec libx264 -profile:v baseline skit.mp4
    //./avconv -f image2 -i %04d.png -vcodec h264 -crf 1 -r 24 out.mp4
    //avconv -framerate 25 -f image2 -i %04d.png -c:v h264 -crf 1 out.mp4
    var params = [
        '-f', 'image2',
       // '-s', '640x480',
       // '-vcodec', 'libx264',
       // '-loglevel', 'info',
        '-i', 'images/cam%09d.jpg',
        '-y', 'video/'+filename,
        //'-start_number', '100',
        '-vcodec', 'h264',
      //  '-c',':v h264',
        '-profile','v',
       // '-crf', '1',
      //  '-pix_fmt', 'yuv420p',
        '-framerate', '25',
        '-r', '24'
        
        

    ];
    // Returns a duplex stream
    var stream = avconv(params);
    // Anytime avconv outputs any information, forward these results to process.stdout
    stream.on('message', function(data) {
     console.log(data);
});
    stream.on('error', function(data) {
        console.log(data);
    });
    stream.on('exit', function(data) {
      console.log('new flow!!')
     
 
        var mongoose = require('mongoose');
       var Grid = require('gridfs-stream');
       Grid.mongo = mongoose.mongo;

var conn = mongoose.createConnection('mongodb://quadrichefa:quadrichefa@ds055792.mongolab.com:55792/quadrichefa');
      conn.once('open', function () {
        
        
      console.log('conn open!!');
      var gfs = Grid(conn.db);

     
    var file = path.resolve('video/'+filename);
    
gfs.remove({
filename: filename
}, function (err) {
  
  console.log('not success');
});



var writestream = gfs.createWriteStream({
                    filename: filename,
                    content_type:'video/mp4'
                });
      var stream = fs.createReadStream(file).pipe(writestream);



   }); 
      
      
      
/*     var file = path.resolve('video/video.mp4');
     var myvideotape= fs.readFileSync(file);
      Article.findOneAndUpdate({title:'quadricam'}, {
       title:'quadricam',
        videotape:myvideotape
      }, {upsert: true},function (err) {
        if (!err) {console.log('Success!');}
        else{
          console.log('err: '+err);
        }
      });
*/
      
    });

  /*
   
 
  var avconv2 = require('avconv');          
   var params2 = [
        '-f', 'image2',
        '-loglevel', 'info',
        '-i', 'images/cam%09d.jpg',
        '-y', 'video/video.webm',
        '-vcodec', 'libx264',
        '-pix_fmt', 'yuv420p',
        '-framerate', '50'
        
    ];
    // Returns a duplex stream
    var stream2 = avconv2(params2);
    // Anytime avconv outputs any information, forward these results to process.stdout
    stream2.on('message', function(data) {
     console.log(data);
});
    stream2.on('error', function(data) {
        console.log(data);
    });
    stream.on('exit', function(data) {
 
     var file = path.resolve('video/video.mp4');
     var myvideotape= fs.readFileSync(file);
      Article.findOneAndUpdate({title:'quadricam'}, {
       title:'quadricam',
       videotape:myvideotape
      }, {upsert: true},function (err) {
        if (!err) {console.log('Success!');}
        else{
          console.log('err: '+err);
        }
      });

      
    });
    
      */    
 }
 else{
   console.log('few images!')
 }
      });
   res.send('req.hostname: '+req.hostname);
  
    
  
};

  
/**
 * convertvideo
 */
exports.convertvideo= function (req, res){
  
    /*
    
     avconv -i "$videoname".mp4 -y "$videoname".webm
   */
   
    var avconv = require('avconv');
    //avconv -i "$videoname".mp4 -y "$videoname".webm
    var params = [
        '-i', 'video/video.mp4',
        '-y', 'video/video.webm'
        
    ];
    // Returns a duplex stream
    var stream = avconv(params);
    // Anytime avconv outputs any information, forward these results to process.stdout
    stream.on('message', function(data) {
     console.log(data);
});
    stream.on('error', function(data) {
        console.log(data);
    });

    
             
   res.send('ciao');
     
    };
  /**
 * getnewimage
 */
exports.rotateimages= function (req, res){

var imagesdir='images/';
var camurl='';

//*******************************************************************************************************//
//http://quadrichefa-001.herokuapp.com
var hostname= req.hostname.replace('.herokuapp.com','').replace('http://','');
switch (hostname) {
    case "quadrichefa-001":
    camurl='http://ipcam.progettosuono.com/webcams/quadri/cam1.jpg';
    break;
    case "borrellochefa-001":
    camurl='http://ipcam.progettosuono.com/webcams/borrello/cam3.jpg';
    break;
    case "civitaluparellachefa-001":
    camurl='http://ipcam.progettosuono.com/webcams/civitaluparella/cam1.jpg';
    break;
    case "lancianochefa-001":
    camurl='http://service.comune.lanciano.chieti.it/webcam/image.jpg';
    break;
    case "pizzoferratochefa-001":
    camurl='http://ipcam.progettosuono.com/webcams/pizzoferrato/cam2.jpg';
    break;
    case "atessachefa-001":
    camurl='http://178.238.53.70:8081/oneshotimage.jpg';
    break;
    case "torricellachefa-001":
    camurl='http://ipcam.progettosuono.com/webcams/torricella/cam2.jpg';
    break;
    case "pescopennatarochefa-001":
    camurl='http://ipcam.progettosuono.com/webcams/pescopennataro/cam1.jpg';
    break;
    case "casolichefa-001":
    camurl='http://ipcam.progettosuono.com/webcams/casoli/cam1.jpg';
    break;
    case "vastochefa-001":
    camurl='http://ipcam.progettosuono.com/webcams/vasto/cam1.jpg';
    break;
    default:
    camurl='http://ipcam.progettosuono.com/webcams/quadri/cam1.jpg';
    
    
     
}
//*******************************************************************************************************//
var http = require('http');
var fs = require('fs');


// 1) get newest and oldest image name
// 2) download and save new image
// 3) if images more than 250 --> delete oldest one
function randomInt (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}
var download = function(url, dest, cb) {
  
  var randovalue=randomInt(1,1000);
  var realurl=url+'?t='+randovalue;
  console.log('realurlrotateimages: '+realurl);
  var file = fs.createWriteStream(dest);
  var request = http.get(realurl, function(response) {
    response.pipe(file);
    file.on('finish', function() {
      file.close(cb);  // close() is async, call cb after close completes.
    });
  }).on('error', function(err) { // Handle errors
 //   fs.unlink(dest); // Delete the file async. (But we don't check the result)
   // if (cb) cb(err.message);
   console.log('error getting image');
  });
}; 

function getNewestFile(dir, files, callback) {
    if (!callback) return;
    if (!files || (files && files.length === 0)) {
        callback();
    }
    if (files.length === 1) {
        callback(files[0]);
    }
    var newest = { file: files[0] };
    var checked = 0;
    fs.stat(dir + newest.file, function(err, stats) {
        newest.mtime = stats.mtime;
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            (function(file) {
                fs.stat(file, function(err, stats) {
                    ++checked;
                    if (stats.mtime.getTime() > newest.mtime.getTime()) {
                        newest = { file : file, mtime : stats.mtime };
                    }
                    if (checked === files.length) {
                        callback(newest);
                    }
                });
            })(dir + file);
        }
    });
 }
 fs.readdir(imagesdir, function (err, files) {
  if (err) throw err;
  //console.log('/usr files: ' + files);
 
  getNewestFile(imagesdir,files, function(cback) {
  var currentimagename=cback.file.replace(imagesdir,'').replace('cam','').replace('.jpg','');
  var  newimagenamedirty='000000000'+(parseInt(currentimagename, 10) + 1);

  var newimagename= newimagenamedirty.substr(newimagenamedirty.length-9);
    
     //   console.log('cback: ' + cback.file);
           
  download(camurl,imagesdir+'cam'+newimagename+'.jpg', function(cback) {
  
         
      }); 
      
  //if (files.length>250) {
//    fs.unlinkSync(imagesdir+files[0]);
//  }
var allfilenames='';
for (var i = 0; i < files.length; i++) {
  // console.log(files[i]);
   allfilenames+= files[i] + ',';
}
   res.send('files.length: '+files.length+' '+allfilenames);
      }); 
});
 
      
      
  


     
    };







     
    //};





