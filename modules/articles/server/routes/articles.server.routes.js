'use strict';

/**
 * Module dependencies.
 */
var articlesPolicy = require('../policies/articles.server.policy'),
  articles = require('../controllers/articles.server.controller');

module.exports = function (app) {
  // Articles collection routes
  app.route('/api/articles').all(articlesPolicy.isAllowed)
    .get(articles.list)
    .post(articles.create);

  // Single article routes
  app.route('/api/articles/:articleId').all(articlesPolicy.isAllowed)
    .get(articles.read)
    .put(articles.update)
    .delete(articles.delete);
    

    app.route('/api/retrofitcatalogcam').all(articlesPolicy.isAllowed)
    .get(articles.getretrofitcatalogcam);
    app.route('/api/newcatalogcam').all(articlesPolicy.isAllowed)
    .get(articles.getnewcatalogcam);
      // Single videocam routes
    app.route('/api/videocam').all(articlesPolicy.isAllowed)
    .get(articles.getvideocam);
    app.route('/api/getvideo').all(articlesPolicy.isAllowed)
    .get(articles.getvideo);
    app.route('/api/getwebmvideo').all(articlesPolicy.isAllowed)
    .get(articles.getwebmvideo);
  // Single videocam routes
    app.route('/api/rotateimages').all(articlesPolicy.isAllowed)
    .get(articles.rotateimages);
    app.route('/api/convertvideo').all(articlesPolicy.isAllowed)
    .get(articles.convertvideo);
    

  // Finish by binding the article middleware
  app.param('articleId', articles.articleByID);
};
